var gulp      = require('gulp');
var sass      = require('gulp-sass');
var coffee    = require('gulp-coffee');
var connect   = require('gulp-connect');
var rename    = require('gulp-rename');
var uglify    = require('gulp-uglify');

var paths = {
  www:    ['./www/**'],
  sass:   ['./scss/**/*.scss'],
  coffee: ['./coffee/**/*.coffee']
};

gulp.task('default', ['sass', 'coffee', 'watch', 'connect']);

gulp.task('sass', function() {
  gulp.src('scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(sass({ outputStyle: 'compressed'} ))
    .pipe(rename({extname: '.min.css'}))
    .pipe(gulp.dest('./www/css/'));
});

gulp.task('coffee', function() {
  gulp.src('coffee/script.coffee')
    .pipe(coffee({bare: true}))
    .pipe(uglify())
    .pipe(rename({extname: '.min.js'}))
    .pipe(gulp.dest('./www/js/'));
});

gulp.task('www', function() {
  gulp.src('www/**')
      .pipe(connect.reload());
});

gulp.task('connect', function() {
  connect.server({
    root: 'www',
    livereload: true
  });
});

gulp.task('watch', function() {
  for (var task in paths) {
    gulp.watch(paths[task], [task]);
  }
});
